emcd :: Int -> Int -> (Int, Int, Int)
emcd a 0 = (a, 1, 0)
emcd a b = (g, s, t)
           where (g, s', t') = emcd b (mod a b)
                 s = t'
                 t = s' - t' * q
                 q = div a b

mcd :: Int -> Int -> Int
mcd a 0 = a
mcd a b = mcd b (mod a b)

--
-- Ejercicio
--

-- Solución mía
solucionEc :: (Int, Int, Int) -> (Int, Int)
solucionEc (a, b, m) | d > 1 && mod b d == 0 = solucionEc (div a d, div b d, div m d)
                     | d > 1 && mod b d > 0 = undefined
                     | a == 1 = (mod b m, m)
                     | otherwise = solucionEc (1, s * b, m)
                     where (d, s, t) = emcd a m

-- Solución profe
{-
ecEquivalente :: (Int, Int, Int) -> (Int, Int, Int)
ecEquivalente (a, b, m) | mod b d /= 0 = undefined
                        | otherwise = (div a d, div b d, div m d)
                        where d = mcd a m

solucionEcConPropAdic :: (Int, Int, Int) -> (Int, Int)
solucionEcConPropAdic (a, b, m) = (mod (s*b) m, m)
                                  where (d, s, t) = emcd a m

solucionEc :: (Int, Int, Int) -> (Int, Int)
solucionEc e = solucionEcConPropAdic (ecEquivalente e)
-}

--
-- Ejercicio
--

sistemaSimplifEquiv :: [(Int, Int, Int)] -> [(Int, Int)]
sistemaSimplifEquiv [] = []
sistemaSimplifEquiv (e:es) = solucionEc e : sistemaSimplifEquiv es

--

menorDivisor :: Int -> Int
menorDivisor n = menorDivisorDesde n 2

menorDivisorDesde :: Int -> Int -> Int
menorDivisorDesde n k | mod n k == 0 = k
                      | otherwise = menorDivisorDesde n (k+1)

esPrimo :: Int -> Bool
esPrimo 1 = False
esPrimo n = (menorDivisor n) == n

--
-- Ejercicio
--

todosLosPrimosMalos :: [(Int, Int)] -> [Int]
todosLosPrimosMalos [] = []
todosLosPrimosMalos sist = todosLosPrimosMalosHasta sist (maximum (modulos sist))

todosLosPrimosMalosHasta :: [(Int, Int)] -> Int -> [Int]
todosLosPrimosMalosHasta _ 0 = []
todosLosPrimosMalosHasta sist n | esPrimoMalo sist n = n:(todosLosPrimosMalosHasta sist (n-1))
                                | otherwise = todosLosPrimosMalosHasta sist (n-1)

esPrimoMalo :: [(Int, Int)] -> Int -> Bool
esPrimoMalo sist n = (esPrimo n) && cantidadMultiplos (modulos sist) n >= 2

modulos :: [(Int, Int)] -> [Int]
modulos [] = []
modulos ((r,m):es) = m:(modulos es)

cantidadMultiplos :: [Int] -> Int -> Int
cantidadMultiplos [] _ = 0
cantidadMultiplos (m:ms) n | mod m n == 0 = 1 + cantidadMultiplos ms n
                           | otherwise = cantidadMultiplos ms n

--
-- Ejercicio
--

solucSistemaPotenciasPrimo :: [(Int, Int)] -> (Int, Int)
solucSistemaPotenciasPrimo [e] = e
solucSistemaPotenciasPrimo (e1:e2:es) = solucSistemaPotenciasPrimo (sole1e2:es)
                                      where sole1e2 = solucSistemaPotenciasPrimo [e1,e2]

solucDosEcPotenciasPrimoOrd :: (Int, Int) -> (Int, Int) -> (Int, Int)
solucDosEcPotenciasPrimoOrd (r1, m1) (r2, m2) | mod (r2-r1) m1 == 0 = (r2, m2)
                                              | otherwise = undefined

solucDosEcPotenciasPrimo :: (Int, Int) -> (Int, Int) -> (Int, Int)
solucDosEcPotenciasPrimo (r1, m1) (r2, m2) | m1 <= m2 = solucDosEcPotenciasPrimoOrd (r1, m1) (r2, m2)
                                           | otherwise = solucDosEcPotenciasPrimoOrd (r2, m2) (r1, m1)

--
-- Ejercicio
--

quePotenciaLoDivide :: Int -> Int -> Int
quePotenciaLoDivide m p | mod m p == 0 = 1 + quePotenciaLoDivide (div m p) p
                        | otherwise = 0

-- No la pude copiar bien, después la tengo que arreglar
{-desdoblarSistemaEnFcionPrimo :: [(Int, Int)] -> Int -> ([(Int, Int)], [(Int, Int)])
desdoblarSistemaEnFcionPrimo [] _ = ([], [])
desdoblarSistemaEnFcionPrimo ((r, m):es) p | k == 0 = (pri, (r, m):seg)
                                           | m == p^k = ((r, m):pri, seg)
                                           | otherwise = ((mod r (p^k):pri), (mod r (div m (p^k)), div m (p^k)):seg))
                                           where (pri, seg) = desdoblarSistemaEnFcionPrimo es p
                                                 k = quePotenciaLoDivide m p
-}

sistemaEquivSinPrimosMalos :: [(Int, Int)] -> [(Int, Int)]
sistemaEquivSinPrimosMalos sist = sistemaEquivSinPrimosMalos sist (todosLosPrimosMalos sist)

sistemaEquivSinPrimosMalosAux :: [(Int, Int)] -> [Int] -> [(Int, Int)]
sistemaEquivSinPrimosMalosAux sist [] = sist
sistemaEquivSinPrimosMalosAux sist (p:ps) = (solucSistemaPotenciasPrimo pri):(sistemaEquivSinPrimosMalosAux seg ps)
                                            where (pri, seg) = desdoblarSistemaEnFcionPrimo sist p


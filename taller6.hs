-----------------------------------
-- Clase 6 del Taller de Álgebra --
-- Lautaro Bernabé Otaño         --
-- 27 de abril de 2022           --
-----------------------------------

--
-- En Haskell las listas son de un solo tipo
-- El orden de las listas es importante ([1,2] != [2,1])
--
--
-- # Funciones importantes:
--
-- head :: [a] -> a
-- Devuelve el primer elemento de [a]
--
-- tail :: [a] -> [a]
-- Devuelve [a] sin el primer elemento
--
--
-- (:) :: a -> [a] -> [a]
-- Devuelve la lista [a] con "a" agregada al principio
-- Cuando se usa varias veces seguidas, separa los términos desde el final
--
-- (++) :: [a] -> [a] -> [a]
-- Devuelve dos listas concatenadas

--
-- Recursión sobre listas
--

-- 1) 
sumatoria :: [Int] -> Int
sumatoria [] = 0
sumatoria l = head l + sumatoria (tail l)

-- 2)
longitud :: [Int] -> Int
longitud [] = 0
longitud l = 1 + longitud (tail l)

-- 3)
pertenece :: Int -> [Int] -> Bool
pertenece _ [] = False
pertenece n l = head l == n || pertenece n (tail l)

--
-- Ejercicios
--

-- *)
-- [1,0..(-100)]

-- *)
primerMultiploDe45345 :: [Int] -> Int
primerMultiploDe45345 l | mod (head l) 45345 == 0 = head l
                        | otherwise = primerMultiploDe45345 (tail l)

--
-- # Pattern matching de listas
--
-- - [] es la lista vacía
-- - (a:l) donde "a" es el primer elemento y "l" es la cola

-- Pertenece con pattern matching
pertenecePM :: Int -> [Int] -> Bool
pertenecePM _ [] = False
pertenecePM n (h:l) = h == n || pertenecePM n l

-- *)
productoria :: [Int] -> Int
productoria l | l == [] = 1
              | otherwise = head l * productoria (tail l)

productoriaPM :: [Int] -> Int
productoriaPM [] = 1
productoriaPM (x:xs) = x * productoriaPM xs

-- *)
sumarN :: Int -> [Int] -> [Int]
sumarN n xs | xs == [] = []
            | otherwise = (head xs + n) : sumarN n (tail xs)

sumarNPM :: Int -> [Int] -> [Int]
sumarNPM _ [] = []
sumarNPM n (x:xs) = (x + n) : sumarNPM n xs

-- *)
sumarElPrimero :: [Int] -> [Int]
sumarElPrimero xs = sumarN (head xs) xs

sumarElPrimeroPM :: [Int] -> [Int]
sumarElPrimeroPM (x:xs) = sumarN x (x:xs)

-- *)
ultimo :: [a] -> a
ultimo (x:[]) = x
ultimo (_:xs) = ultimo xs

sumarElUltimo :: [Int] -> [Int]
sumarElUltimo xs = sumarN (ultimo xs) xs

-- *)
pares :: [Int] -> [Int]
pares xs | xs == [] = []
         | mod (head xs) 2 == 0 = (head xs) : pares (tail xs)
         | otherwise = pares(tail xs)

paresPM :: [Int] -> [Int]
paresPM (_:[]) = []
paresPM (x:xs) | mod x 2 == 0 = x : paresPM xs
               | otherwise = pares xs

-- *)
multiplosDeN :: Int -> [Int] -> [Int]
multiplosDeN n xs | xs == [] = []
                  | mod (head xs) n == 0 = (head xs) : multiplosDeN n (tail xs)
                  | otherwise = multiplosDeN n (tail xs)

multiplosDeNPM :: Int -> [Int] -> [Int]
multiplosDeNPM _ (x:[]) = [x]
multiplosDeNPM n (x:xs) | mod x n == 0 = x : multiplosDeNPM n xs
                        | otherwise = multiplosDeNPM n xs

-- *)
quitar :: Int -> [Int] -> [Int]
quitar n xs | xs == [] = []
            | head xs == n = tail xs
            | otherwise = head xs : quitar n (tail xs)

quitarPM :: Int -> [Int] -> [Int]
quitarPM _ (x:[]) = [x]
quitarPM n (x:xs) | x == n = xs
                  | otherwise = x : quitarPM n xs

-- *)
hayRepetidos :: [Int] -> Bool
hayRepetidos xs | xs == [] = False
hayRepetidos xs | otherwise = pertenece (head xs) (tail xs) || hayRepetidos (tail xs)

hayRepetidosPM :: [Int] -> Bool
hayRepetidosPM (_:[]) = False
hayRepetidosPM (x:xs) = pertenecePM x xs || hayRepetidosPM xs

-- *)
quitarTodos :: Int -> [Int] -> [Int]
quitarTodos n xs | xs == [] = []
                 | n == head xs = quitarTodos n (tail xs)
                 | otherwise = head xs : quitarTodos n (tail xs)

eliminarRepetidos :: [Int] -> [Int]
eliminarRepetidos xs | xs == [] = []
                     | otherwise = head xs : eliminarRepetidos (quitarTodos (head xs) (tail xs))

quitarTodosPM :: Int -> [Int] -> [Int]
quitarTodosPM _ [] = []
quitarTodosPM n (x:xs) | n == x = quitarTodos n xs
                       | otherwise = x : quitarTodos n xs

eliminarRepetidosPM :: [Int] -> [Int]
eliminarRepetidosPM [] = []
eliminarRepetidosPM (x:xs) = x : eliminarRepetidos (quitarTodos x xs)

-- *)
-- minBound :: Int es el menor Int posible

masGrandeDesde :: Int -> [Int] -> Int
masGrandeDesde n xs | xs == [] = n
                  | head xs > n = masGrandeDesde (head xs) (tail xs)
                  | otherwise = masGrandeDesde n (tail xs)

maximo :: [Int] -> Int
maximo xs = masGrandeDesde (minBound :: Int) xs

masGrandeDesdePM :: Int -> [Int] -> Int
masGrandeDesdePM n [] = n
masGrandeDesdePM n (x:xs) | x > n = masGrandeDesdePM x xs
                          | otherwise = masGrandeDesdePM n xs

maximoPM :: [Int] -> Int
maximoPM xs = masGrandeDesdePM (minBound :: Int) xs

-- *)
ordenarDesc :: [Int] -> [Int]
ordenarDesc xs | xs == [] = []
               | otherwise = max : ordenarDesc (quitar max xs)
               where max = maximo xs

ordenar :: [Int] -> [Int]
ordenar xs = reverso (ordenarDesc xs)

ordenarDescPM :: [Int] -> [Int]
ordenarDescPM [] = []
ordenarDescPM xs = max : ordenarDesc (quitar max xs)
                   where max = maximo xs

ordenarPM :: [Int] -> [Int]
ordenarPM xs = reversoPM (ordenarDescPM xs)

-- *)
reverso :: [Int] -> [Int]
reverso xs | xs == [] = []
           | otherwise = reverso (tail xs) ++ [head xs]

reversoPM :: [Int] -> [Int]
reversoPM [] = []
reversoPM (x:xs) = reverso xs ++ [x]

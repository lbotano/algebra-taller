absoluto n | n > 0 = n
           | otherwise = -n

maximoabsoluto a b | a' > b' = a'
                   | otherwise = b'
                   where a' = absoluto a
                         b' = absoluto b

maximo3 a b c | a > b && a > c = a
              | b > a && b > c = b
              | otherwise = c

algunoEs0 a b = a * b == 0

algunoEs0' 0 _ = True
algunoEs0' _ 0 = True
algunoEs0' a b = False

esMultiploDe a b = mod b a == 0

digitoUnidades :: Int -> Int
digitoUnidades a = mod a 10

digitoDecenas :: Int -> Int
digitoDecenas a = mod (div a 10) 10

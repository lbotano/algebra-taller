-----------------------------------
-- Clase 5 del Taller de Álgebra --
-- Lautaro Bernabé Otaño         --
-- 20 de abril de 2022           --
-----------------------------------


--
-- Ejercicios
--

-- 1)
sumaDivisoresHasta :: Int -> Int -> Int
sumaDivisoresHasta n k | k == 1 = 1
                       | mod n k == 0 = k + sumaDivisoresHasta n (k - 1)
                       | otherwise = sumaDivisoresHasta n (k - 1)

-- 2)
sumaDivisores :: Int -> Int
sumaDivisores n = sumaDivisoresHasta n n

-- 3)
menorDivisorDesde :: Int -> Int -> Int
menorDivisorDesde n l | mod n l == 0 = l
                      | otherwise = menorDivisorDesde n (l + 1)

menorDivisor :: Int -> Int
menorDivisor n = menorDivisorDesde n 2

-- 4)
esPrimo :: Int -> Bool
esPrimo n = menorDivisor n == n

-- 5)
primerPrimoDesde :: Int -> Int
primerPrimoDesde n | esPrimo n = n
                   | otherwise = primerPrimoDesde (n + 1)

nEsimoPrimo :: Int -> Int
nEsimoPrimo 1 = 2 
nEsimoPrimo n = primerPrimoDesde (nEsimoPrimo (n - 1) + 1)

-- 6)

-- menorFactDesde m = min { k!, k / k! >= m }

factorial :: Int -> Int
factorial 1 = 1
factorial n = n * factorial (n - 1)

menorFactDesdeAux :: Int -> Int -> Int
menorFactDesdeAux m k | factorial k < m = menorFactDesdeAux m (k + 1)
                      | otherwise = factorial k

menorFactDesde :: Int -> Int
menorFactDesde m = menorFactDesdeAux m 1

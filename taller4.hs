-----------------------------------
-- Clase 4 del Taller de Álgebra --
-- Lautaro Bernabé Otaño         --
-- 13 de abril de 2022           --
-----------------------------------

sumatoria :: Integer -> Integer
sumatoria 1 = 1
sumatoria n = n + sumatoria (n - 1)

--
-- Ejercicios: otras sumatorias
--

-- 1)
f1 :: Integer -> Integer
f1 0 = 1
f1 n = 2^n + f1(n - 1)

-- 2)
f2 :: Integer -> Float -> Float
f2 1 q = q
f2 n q = q^n + f2 (n - 1) q

-- 3)
f3 :: Integer -> Float -> Float
f3 0 _ = 0
f3 1 q = q + q^2
f3 n q = q^(2*n) + q^(2*n - 1) + f3 (n - 1) q

-- 4)
f4 :: Integer -> Float -> Float
f4 0 q = 1
f4 n q = q^(2*n-1) + q^(2*n) - q^(n-1) + f4 (n-1) q

--
-- Implementar eAprox :: Integer -> Float que aproxime el valor de "e"
--
fact :: Integer -> Integer
fact 0 = 1
fact n = n * fact (n - 1)

eAprox :: Integer -> Float
eAprox 0 = 1
eAprox n = 1/fromIntegral(fact n) + eAprox (n - 1)

e = eAprox 10

--
-- Ejercicios: sumatorias dobles
--

-- 1)

-- f(n, m) = 1¹     + 1²     + ... + 1^m +     ||
--           2¹     + 2²     + ... + 2^m +     ||
--           .                                 ||
--           .                                 ||
--           .                                 ||
--           (n-1)¹ + (n-1)² + ... + (n-1)^m + ||
--           n¹     + n²     + ... + n^m

f :: Integer -> Integer -> Float
f 0 m = 0
f n m = f2 m (fromIntegral n) + f (n - 1) m

-- 2)
sumaPotencias :: Float -> Integer -> Integer -> Float
sumaPotencias q 1 m = q * f2 m q
sumaPotencias q n m = q^n * f2 m q + sumaPotencias q (n - 1) m

-- 3)
sumaRacionales :: Integer -> Integer -> Float
sumaRacionales n 1 = fromInteger (sumatoria n)
sumaRacionales n m = (1 / fromInteger m) * fromInteger (sumatoria n) + sumaRacionales n (m - 1)

--
-- Ejercicios de tarea
--

-- 4)
g1 :: Integer -> Integer -> Integer
g1 i n | n == i = i^n
       | otherwise = i^n + g1 i (n - 1)

-- 5)
g2Aux :: Integer -> Integer -> Integer
g2Aux 1 b = 1
g2Aux a b = a^b + g2Aux (a - 1) b

g2 :: Integer -> Integer
g2 1 = 1
g2 n = g2Aux n n + g2 (n - 1)

-- 6)
g3 :: Integer -> Integer
g3 n | n <= 1 = 0
     | even n = 2^n + g3 (n - 2)
     | otherwise = g3 (n - 1)

-- 7)
digitosSonIguales :: Integer -> Bool
digitosSonIguales n | n < 10 = True
                    | mod n 10 == mod (div n 10) 10 = digitosSonIguales (div n 10)
                    | otherwise = False

sumNatDigitosIguales :: Integer -> Integer
sumNatDigitosIguales n | n == 1 = 1
                       | digitosSonIguales n = n + sumNatDigitosIguales (n - 1)
                       | otherwise = sumNatDigitosIguales(n - 1)

-----------------------------------
-- Clase 9 del Taller de Álgebra --
-- Lautaro Bernabé Otaño         --
-- 1 de junio de 2022            --
-----------------------------------

-- a, d, q, r
division :: Int -> Int -> (Int, Int)
division a d | 0 <= a && a < d = (0, a)
             | a < 0 = (fst qr'' - 1, snd qr'')
             | otherwise = (fst qr' + 1, snd qr')
             where qr'  = division (a - d) d
                   qr'' = division (a + d) d

--
-- Ejercicios
--

-- 1)
mcd :: Int -> Int -> Int
mcd a 0 = a
mcd a b = mcd b (mod a b)

-- 2)
menorDivisorDesde :: Int -> Int -> Int
menorDivisorDesde n l | mod n l == 0 = l
                      | otherwise = menorDivisorDesde n (l + 1)

menorDivisor :: Int -> Int
menorDivisor 1 = 1
menorDivisor n = menorDivisorDesde n 2

mayorDivisorComun :: Int -> Int -> Int
mayorDivisorComun a 0 = a
mayorDivisorComun _ 1 = 1
mayorDivisorComun 1 _ = 1
mayorDivisorComun a b | menorDivisor a == menorDivisor b = menorDivisor a *
                          mayorDivisorComun (div a (menorDivisor a)) (div b (menorDivisor b))
                      | menorDivisor a > menorDivisor b =
                          mayorDivisorComun a (div b (menorDivisor b))
                      | otherwise = mayorDivisorComun (div a (menorDivisor a)) b

-- ESTE ES FEO FEO MUY FEO NO USAR EN VIDA REAL
-- con :set +s se puede probar que es mucho más ineficiente que mcd o mayorDivisorComun
mcd'' :: Int -> Int -> Int
mcd'' a b = mcdHasta a b (min a b)

mcdHasta :: Int -> Int -> Int -> Int
mcdHasta a b c | mod a c == 0 && mod b c == 0 = c
               | otherwise = mcdHasta a b (c - 1)

--
-- Algoritmo extendido de Euclides
--

fst3 (x, _, _) = x
snd3 (_, y, _) = y
trd3 (_, _, z) = z

--- Devuelve ((a : b), s, t) con s*a + t*b = (a : b)
emcd :: Int -> Int -> (Int, Int, Int)
emcd a 0 = (a, 1, 0)
emcd a b = (g, s, t)
           where (g, s', t') = emcd b (mod a b)
                 s = t'
                 t = s' - t' * (div a b)

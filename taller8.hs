----------------------------------
-- Clase 8 del Taller de Álgebra --
-- Lautaro Bernabé Otaño         --
-- 11 de mayo de 2022            --
-----------------------------------

--- Funciones de la clase anterior ---
type Set a = [a]

agregar :: (Eq a) => a -> Set a -> Set a
agregar n c | elem n c = c
            | otherwise = n : c

union :: Eq a => Set a -> Set a -> Set a
union [] c2 = c2
union (h1:t1) c2 | elem h1 c2 = (union t1 c2)
                 | otherwise = h1:(union t1 c2)
--------------------------------------

combinatorio :: Int -> Int -> Int
combinatorio n m | m == 0 || m == n = 1
                 | otherwise = combinatorio (n - 1) m + combinatorio (n - 1) (m - 1)

---

agregarAdelante :: Int -> Set [Int] -> Set [Int]
agregarAdelante _ [] = []
agregarAdelante x (ys:yss) = agregar (x:ys) (agregarAdelante x yss)

agregarAListas :: Set Int -> Set [Int] -> Set [Int]
agregarAListas [] _ = []
agregarAListas (x:xs) c = union (agregarAdelante x c) (agregarAListas xs c)

variaciones :: Set Int -> Int -> Set [Int]
variaciones c 0 = [[]]
variaciones c l = agregarAListas c (variaciones c (l - 1))

---

insertarEn :: [Int] -> Int -> Int -> [Int]
insertarEn l n 1 = n : l
insertarEn (hl:tl) n i = hl : insertarEn tl n (i - 1)

---

-- Tenemos que hacer insertarEn desde 1 a n, para cada lista de las permutaciones de n-1

-- n: numero a insertar
-- l: permutacion posible
-- i: indice, empieza = n
posiblesInsertaciones :: Int -> [Int] -> Int -> Set [Int]
posiblesInsertaciones n l 1 = [n : l]
posiblesInsertaciones n l i = union [(insertarEn l n i)] (posiblesInsertaciones n l (i-1))

-- n es la permutacion actual
-- m es la lista de permutaciones de (n-1) (voy sacando de a uno)
permutacionesAux :: Int -> Set [Int] -> Set [Int]
permutacionesAux n [] = []
permutacionesAux n (m:xm) = union (posiblesInsertaciones n m n) (permutacionesAux n xm)

permutaciones :: Int -> Set [Int]
permutaciones 0 = [[]]
permutaciones n = permutacionesAux n (permutaciones (n - 1))

-----------------------------------
-- Clase 7 del Taller de Álgebra --
-- Lautaro Bernabé Otaño         --
-- 4 de mayo de 2022             --
-----------------------------------

type Set a = [a]

conjuntoEjemplo :: Set Int
conjuntoEjemplo = [1, 3, 4, 7]

vacio :: Set Int
vacio = []

agregar :: Int -> Set Int -> Set Int
agregar n c | elem n c = c
            | otherwise = n : c

--
-- Ejercicios simples
--

-- 1)
incluido :: Set Int -> Set Int -> Bool
incluido [] _ = True
incluido (h1:t1) c2 | elem h1 c2 = incluido t1 c2
                    | otherwise = False

-- 2)
iguales :: Set Int -> Set Int -> Bool
iguales c1 c2 = incluido c1 c2 && incluido c2 c1

-- *)
union :: Set Int -> Set Int -> Set Int
union [] c2 = c2
union (h1:t1) c2 | elem h1 c2 = (union t1 c2)
                 | otherwise = h1:(union t1 c2)

-- *)
interseccion :: Set Int -> Set Int -> Set Int
interseccion [] _ = []
interseccion (h1:t1) c2 | elem h1 c2 = h1:interseccion t1 c2
                        | otherwise = interseccion t1 c2

--
-- Ejercicios
--

-- 1)
-- agregarAHijos recibe n y [[a,b,c], [x,y,z], [foo,bar], []]
-- y devuelve [[n,a,b,c], [n,x,y,z], [n,foo,bar], [n]]
agregarAHijos :: Int -> Set (Set Int) -> Set (Set Int)
agregarAHijos n [] = []
agregarAHijos n (x:xs) = (n:x):(agregarAHijos n xs)

partes :: Int -> Set (Set Int)
partes 1 = [[], [1]]
partes n = partesAnterior ++ (agregarAHijos n partesAnterior)
           where partesAnterior = partes (n-1)

-- *)
generarTuplas :: Int -> Set Int -> Set (Int, Int)
generarTuplas n [] = []
generarTuplas n (x:xs) = (n, x) : generarTuplas n xs

productoCartesiano :: Set Int -> Set Int -> Set (Int, Int)
productoCartesiano [] _ = []
productoCartesiano (x:xs) b = generarTuplas x b ++ productoCartesiano xs b

-----------------------------------
-- Clase 2 del Taller de Álgebra --
-- Lautaro Bernabé Otaño         --
-- 6 de abril de 2022            --
-----------------------------------

factorial :: Int -> Int
factorial n | n == 0 = 1
            | otherwise = n * factorial (n - 1)

esPar :: Int -> Bool
esPar n | n == 0 = True
        | otherwise = not (esPar (n - 1))

--
-- Ejercicios
--

-- 1)
fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

-- 2)
parteEntera :: Float -> Integer
parteEntera n | n < 1 = 0
              | otherwise = parteEntera(n - 1) + 1

--
-- Más ejercicios
--

-- 1)
esMultiploDe3 :: Integer -> Bool
esMultiploDe3 n | n == 0 = True
                | n < 0 = False
                | otherwise = esMultiploDe3 (n - 3)

-- 2)
sumaImpares :: Int -> Int
sumaImpares 0 = 0
sumaImpares n = (2 * n - 1) + sumaImpares(n - 1) 

-- 3)
medioFact :: Int -> Int
medioFact 0 = 1
medioFact 1 = 1
medioFact n = n * medioFact (n - 2)

--
-- Más ejercicios pt. 2
--

-- 1)
sumaDigitos :: Int -> Int
sumaDigitos 0 = 0
sumaDigitos n = mod n 10 + sumaDigitos(div n 10)

-- 2)
sonDigitosIguales :: Int -> Bool
sonDigitosIguales n | n < 10 = True
                    | otherwise = mod n 10 == div (mod n 100) 10 && sonDigitosIguales(div n 10)

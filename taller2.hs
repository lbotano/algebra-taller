-----------------------------------
-- Clase 2 del Taller de Álgebra --
-- Lautaro Bernabé Otaño         --
-- 30 de marzo de 2022			 --
-----------------------------------

-- Da True si son el mismo tipo de dato
-- Tira error si son tipos de datos distintos
mismoTipo :: t -> t -> Bool
mismoTipo x y = True

-- Da True siempre, independientemente de los tipos de dato
mismoTipo' x y = True

-- Las operaciones aritméticas restringen los tipos de argumentos
triple x = 3*x

maximo x y | x >= y = x
           | otherwise = y

-- Devuelve bool
f1 x y z = x**y + z <= x+y**z

-- Devuelve float
f2 x y = (sqrt x) / (sqrt y)


f3 x y = div (sqrt x) (sqrt y)

-- Devuelve float
f4 x y z | x == y = z
         | x ** y == y = x
         | otherwise = y

f5 x y z | x == y = z
         | x ** y == y = z
         | otherwise = z

-- Devuelve la suma entre dos tuplas
suma :: (Float, Float) -> (Float, Float) -> (Float, Float)
suma v w = ((fst v) + (fst w), (snd v) + (snd w))

-- Usando pattern matching
suma' (vx, vy) (wx, wy) = (vx + wx, vy + wy)

-- Usar pattern matching sobre constructores de tuplas y números
esOrigen :: (Float,Float) -> Bool
esOrigen (0, 0) = True
esOrigen (_, _) = False

--
-- EJERCICIOS
--

-- 1)
estanRelacionados :: Float -> Float -> Bool
estanRelacionados x y | x <= 3 && y <= 3 = True
                      | x > 3 && y > 3 && x <= 7 && y <= 7 = True
                      | x > 7 && y > 7 = True
                      | otherwise = False

-- 2)
prodInt :: (Float, Float) -> (Float, Float) -> Float
prodInt (x1, y1) (x2, y2) = x1 * x2 + y1 * y2

-- 3)
todoMenor :: (Float, Float) -> (Float, Float) -> Bool
todoMenor (x1, y1) (x2, y2) = x1 < x2 && y1 < y2

-- 4)
distanciaPuntos :: (Float, Float) -> (Float, Float) -> Float
distanciaPuntos (x1, y1) (x2, y2) = sqrt ((x2 - x1) ** 2 + (y2 - y1) ** 2)

-- 5)
sumaTerna :: (Integer, Integer, Integer) -> Integer
sumaTerna (a, b, c) = a + b + c

-- 6)
posicPrimerPar :: (Integer, Integer, Integer) -> Integer
posicPrimerPar (a, b, c) | mod a 2 == 0 = 1
                         | mod b 2 == 0 = 2
                         | mod c 2 == 0 = 3
                         | otherwise = 4

-- 7)
crearPar :: a -> b -> (a, b)
crearPar a b = (a, b)

-- 8)
invertir :: (a, b) -> (b, a)
invertir (a, b) = (b, a)
